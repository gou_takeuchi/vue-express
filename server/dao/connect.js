var mongoose = require('mongoose');

//mongoose.connect('mongodb://localhost:27017/fd', {useNewUrlParser: true},
mongoose.connect('mongodb://127.0.0.1:27017/fd', {useNewUrlParser: true},
  function(err){
    if(err){
        console.error(err);
    }else{
        console.log("Successfully connected to MongoDB.");
    }
});

module.exports = mongoose;
