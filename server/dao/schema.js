var mongoose = require('mongoose');

var empSchema = mongoose.Schema({
  執行役員就任日: String,
  社員番号: String,
  セイ: String,
  メイ: String,
  姓: String,
  名: String,
  本部: String,
  部門: String,
  部課: String,
  グループチーム: String,
  役職: String,
  入社年: Number,
  メールアドレスGmail: String,
  携帯電話番号: String,
  メールアドレス携帯電話: String,
  Googleリンク: String,
  不明: String,
  顔写真: String
}, {collection: 'payroll'});

module.exports = mongoose.model('Payroll',empSchema)
